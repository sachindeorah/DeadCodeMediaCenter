//
//  MCPlayerViewController.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

/**
 *  MCPlayerViewController Class is responsible for controlling the application content viewer.
 *  It is subclass UIViewController interating with MCPlayerView which provides the playable View as per content type
 */

import UIKit

class MCPlayerViewController: UIViewController {

    var medias: Array<MCMedia>?
    var comingView: UIView?
    var outGoingView: UIView?
    
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var leftArrow: UIButton!
    @IBOutlet weak var rightArrow: UIButton!
    
    
    var mediaIndex = 0
    
    var mcPlayerView = MCPlayerView.loadFromNibNamed(kMCPlayerView) as? MCPlayerView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.hidden = true
        
        
        let media = medias![mediaIndex]
        
        mcPlayerView!.frame = CGRectMake(2, 2, containerView.frame.size.width - 4, containerView.frame.size.height - 4)
        containerView.addSubview(mcPlayerView!.getViewWithMedia(media))
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MCPlayerViewController.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)


    }
    
    func rotated()
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Button Click Actions
    
    /**
     * @brief To navigate back to parent view controller
     * @param sender is the object which sent the message to that selector.
     * @return void
     */
    @IBAction func backButtonAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /**
     * @brief click action to view left side of the content
     * @param sender is the object which sent the message to that selector.
     * @return void
     */
    @IBAction func leftArrowAction(sender: AnyObject) {
        
        if mediaIndex == 0 {
            return
        }
        mediaIndex = mediaIndex - 1
        
        let media = medias![mediaIndex]
        print("\(mediaIndex)", media)
        mcPlayerView?.player.pause()
        animationComingFromRight()

    }
    
    /**
     * @brief click action to view right side of the content
     * @param sender is the object which sent the message to that selector.
     * @return void
     */
    @IBAction func rightArrowAction(sender: AnyObject) {
        
        if mediaIndex == (medias?.count)! - 1 {
            return
        }
        mediaIndex = mediaIndex + 1
        mcPlayerView?.player.pause()
        animationComingFromLeft()
        
    }
    
    //MARK: Animation
    
    /**
     * @brief Translate Custome Player View with holder from left to right
     * @return void
     */
    func animationComingFromLeft() {
        let media = medias![mediaIndex]
        mcPlayerView = MCPlayerView.loadFromNibNamed(kMCPlayerView) as? MCPlayerView
        mcPlayerView!.frame = CGRectMake(containerView.frame.size.width - 4, 2, containerView.frame.size.width - 4, containerView.frame.size.height - 4)
        containerView.addSubview(mcPlayerView!.getViewWithMedia(media))
        UIView.animateWithDuration(0.8) { 
            self.mcPlayerView?.frame = CGRectMake(2, 2, self.containerView.frame.size.width - 4, self.containerView.frame.size.height - 4)
        }
    }
    
    /**
    * @brief Translate Custome Player View with holder right to left
    * @return void
    */
    
    func animationComingFromRight() {
        let media = medias![mediaIndex]
        mcPlayerView = MCPlayerView.loadFromNibNamed(kMCPlayerView) as? MCPlayerView
        mcPlayerView!.frame = CGRectMake(-(containerView.frame.size.width - 4), 2, containerView.frame.size.width - 4, containerView.frame.size.height - 4)
        containerView.addSubview(mcPlayerView!.getViewWithMedia(media))
        UIView.animateWithDuration(0.8) {
            self.mcPlayerView?.frame = CGRectMake(2, 2, self.containerView.frame.size.width - 4, self.containerView.frame.size.height - 4)
        }

    }
    
}
