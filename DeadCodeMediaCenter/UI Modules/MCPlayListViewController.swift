//
//  MCPlayListViewController.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit
/**
 *  Showing selected media as PlayList
 */
class MCPlayListViewController: UIViewController {
    var playLists: [CDPlayList] = []
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.backgroundColor = UIColor.clearColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = false
        playLists = MCCoreDataManager().fetchAllPlayListFromDB()
        print(playLists)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UITABLEVIEW DATASOURCE
    
    internal func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return playLists.count;
    }
    
    internal func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kReuseIdentifier, forIndexPath: indexPath)
        
        let theCDPlaylist:CDPlayList = playLists[indexPath.row]
        cell.textLabel?.text = theCDPlaylist.playListName
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableView.separatorColor = UIColor.grayColor()
        
        
        return cell
    }
    
    internal func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let playerViewController = self.storyboard?.instantiateViewControllerWithIdentifier(kMCPlayerViewController) as! MCPlayerViewController!
        let theCDPlaylist = playLists[indexPath.row]
        let cdmediaList: [CDMedia] = theCDPlaylist.medias?.allObjects as! [CDMedia]
        playerViewController.medias = MCCoreDataManager().cdMediaToMcMediaModelConverter(cdmediaList)
        self.navigationController?.showViewController(playerViewController, sender: nil)
        playerViewController.hidesBottomBarWhenPushed = true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // Action to delete data
            MCCoreDataManager().deletePlaylistFromDB(playLists[indexPath.row])
            playLists = MCCoreDataManager().fetchAllPlayListFromDB()
            self.tableView.reloadData()
            
        }
    }

    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
    }
}
