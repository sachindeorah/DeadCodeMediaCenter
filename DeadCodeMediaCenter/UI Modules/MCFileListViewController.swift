//
//  MCFileListViewController.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit

/**
 *  MCFileListViewController Class is responsible various type of file content.
 *  It is subclass UIViewController, confirming to UITableViewDelegate and UITableViewDataSource interating with MCPlayerViewController to view single or multiple content. This also extract the content from Bundle where, media files are stored.
 */

class MCFileListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var fileListTableView: UITableView!
    
    var browserFileList: [MCBrowserModel] = []
    
    
    @IBOutlet weak var selectFileButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    var isSelctModeOn = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UIApplication.sharedApplication().delegate as! AppDelegate)
        self.view.backgroundColor = UIColor(colorLiteralRed: 115/255, green: 115/255, blue: 115/255, alpha: 1)
        self.fileListTableView.backgroundColor = UIColor(colorLiteralRed: 115/255, green: 115/255, blue: 115/255, alpha: 1)
        
       let fileArray =  MCFileManager.getAllFilesFromResourceBundle()
        
        var mediaList: [MCMedia] = []
        
        for fileName in fileArray {
            let media: MCMedia = MCMedia.init(fileName: fileName)
            mediaList.append(media)
        }
        

        
        let onlyAudios = mediaList.filter({
            $0.fileType == MediaType.audio
        })
        let bowserObject1: MCBrowserModel = MCBrowserModel.init(fileType: MediaType.audio, medias: onlyAudios)

        
        let onlyImages = mediaList.filter({
            $0.fileType == MediaType.image
        })
        let bowserObject4: MCBrowserModel = MCBrowserModel.init(fileType: MediaType.image, medias: onlyImages)

        
        let onlyVideos = mediaList.filter({
            $0.fileType == MediaType.video
        })
        let bowserObject2: MCBrowserModel = MCBrowserModel.init(fileType: MediaType.video, medias: onlyVideos)
        
        let onlyDocuments = mediaList.filter({
            $0.fileType == MediaType.document
        })
        let bowserObject3: MCBrowserModel = MCBrowserModel.init(fileType: MediaType.document, medias: onlyDocuments)

        browserFileList = [bowserObject1, bowserObject2, bowserObject3, bowserObject4]
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MCFileListViewController.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)

    }

    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false
    }
    
    func rotated()
    {
        fileListTableView!.reloadData()
        fileListTableView!.reloadInputViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegates And Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return browserFileList.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if(browserFileList[section].isSectionOpen == true)
        {
            return browserFileList[section].medias!.count
        }
        return 0;
        
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = kBrowserCell
        let cell: MCFileTableViewCell! = self.fileListTableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! MCFileTableViewCell
        cell.backgroundColor = UIColor(colorLiteralRed: 115/255, green: 115/255, blue: 115/255, alpha: 1)
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let browserObj = browserFileList[indexPath.section]

        
        if (!browserObj.isSectionOpen) {
            //  cell.textLabel.text = @"click to enlarge";
        }
        else{
            let media = browserObj.medias![indexPath.row]
            cell.fileName?.text = media.displayName
            cell.textLabel?.textColor = UIColor.whiteColor()
            if selectFileButton.selected == true {
                if media.isSelectedToPlay == true {
                    cell.selectIcon.image = UIImage.init(named: "tick_Share_blue")
                }
                else {
                    cell.selectIcon.image = UIImage.init(named: "tick_Share_grey_disabled")
                }
            }
        }
        
        if selectFileButton.selected == true {
            cell.selectIcon.hidden = false
        }
        else {
            cell.selectIcon.hidden = true
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let browserObj = browserFileList[indexPath.section]
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! MCFileTableViewCell

        if selectFileButton.selected == true {
            
            browserObj.medias![indexPath.row].isSelectedToPlay = !browserObj.medias![indexPath.row].isSelectedToPlay
            if browserObj.medias![indexPath.row].isSelectedToPlay == true {
                cell.selectIcon.image = UIImage.init(named: "tick_Share_blue")
            }
            else {
                cell.selectIcon.image = UIImage.init(named: "tick_Share_grey_disabled")
            }
        }
        else {
            let playerViewController = self.storyboard?.instantiateViewControllerWithIdentifier(kMCPlayerViewController) as! MCPlayerViewController!
            playerViewController.medias = [browserObj.medias![indexPath.row]]
            self.navigationController?.showViewController(playerViewController, sender: nil)
            playerViewController.hidesBottomBarWhenPushed = true
        }
        
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 10))
        headerView.backgroundColor = UIColor(colorLiteralRed: 115/255, green: 115/255, blue: 115/255, alpha: 1)
        headerView.tag = section
        
        let headerBaseLine = UIView(frame: CGRect(x: 10, y: 90, width: tableView.frame.size.width - 20, height: 1)) as UIView
        let headerIcon = UIImageView(frame: CGRect(x: 20, y: 5, width: 60, height: 74)) as UIImageView
        headerIcon.image = UIImage(named:MCMedia.fileIconFinder(browserFileList[section].fileType!))
        headerBaseLine.backgroundColor = UIColor.whiteColor()
        let headerString = UILabel(frame: CGRect(x: 95, y: 5, width: tableView.frame.size.width - 20, height: 90)) as UILabel
        headerString.text = browserFileList[section].fileType!.rawValue
        headerString.font = UIFont(name: "OpenSans-Regular", size: 14)
        headerString.textColor = UIColor.whiteColor()
        headerString.backgroundColor = UIColor.clearColor()
        headerView .addSubview(headerString)
        headerView .addSubview(headerBaseLine)
        headerView .addSubview(headerIcon)
        let headerTapped = UITapGestureRecognizer (target: self, action:#selector(MCFileListViewController.sectionHeaderTapped(_:)))
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView
    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        
        let indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        
        if (indexPath.row == 0) {
            
            let browserObj = browserFileList[indexPath.section]
            
            browserObj.isSectionOpen = !browserObj.isSectionOpen
            let range = NSMakeRange(indexPath.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.fileListTableView.reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
        
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 100
    }

    //MARK: Click Actions
    
    @IBAction func playButtonAction(sender: UIButton) {
        
        
        var selectedMediaFiles: [MCMedia] = []
        
        
        for browser in browserFileList {
            for media in browser.medias! {
                if media.isSelectedToPlay == true {
                    selectedMediaFiles.append(media)
                }
            }
        }
        
        if selectedMediaFiles.count > 0 {
            
            let userDefault = NSUserDefaults.standardUserDefaults()
            var listCount = 0
            
            if (userDefault.valueForKey(kIncrementor)) != nil {
                listCount = (userDefault.valueForKey(kIncrementor)) as! Int
            }
            else {
                listCount = 0
            }
            
            if listCount == 0 {
                
                let coreDataManager = MCCoreDataManager()
                if coreDataManager.fetchAllPlayListFromDB().count > 0 {
                    listCount = listCount + 1
                    userDefault.setInteger(listCount, forKey: kIncrementor)
                    userDefault.synchronize()
                }
            }
            else {
                listCount = listCount + 1
                userDefault.setInteger(listCount, forKey: kIncrementor)
                userDefault.synchronize()
            }
            
            let coreDataManager = MCCoreDataManager()
            coreDataManager.insertPlaylistIntoDB("\(kPlaylist) \(listCount)", medias: selectedMediaFiles)
            
            let playerViewController = self.storyboard?.instantiateViewControllerWithIdentifier(kMCPlayerViewController) as! MCPlayerViewController!
            playerViewController.medias = selectedMediaFiles
            self.navigationController?.showViewController(playerViewController, sender: nil)
            playerViewController.hidesBottomBarWhenPushed = true
        }
        else {
            let alert:UIAlertController=UIAlertController(title:kAlert, message: kAlertMessageForPlaying, preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title:kOK, style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectButtonAction(sender: UIButton) {
        selectFileButton.selected = !selectFileButton.selected
        fileListTableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
