//
//  MCFileManager.swift
//  DeadCodeMediaCenter
//
//  Created by Bhuvanendra Maurya on 7/16/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit

class MCFileManager: NSObject {
    
    /**
     * Get Document Directory Path
     * @return: [SString]
     */
    class func getDocumentsDirectory() -> NSString {
        let documentsDirectory = NSBundle.mainBundle().pathForResource("Resources", ofType: "bundle")
        return documentsDirectory!
    }
    
    /**
     * Getting all files from Bundle
     * @return: [String]
     */
    class func getAllFilesFromResourceBundle() -> [String] {
        
        let documentsDirectory = getDocumentsDirectory()
        
        do {
            let directory =  try NSFileManager.defaultManager().contentsOfDirectoryAtPath(documentsDirectory as String)
            return directory
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        return []
    }
    
    /**
     * Extracting file extension
     * @return: String
     */
    class func extentionExtractor(fileName: String) -> String{
        let fileNameComponent = fileName.characters.split{$0 == "."}.map(String.init)
        return fileNameComponent.last!
    }
    
    /**
     * Extracting file name only
     * @return: String
     */
    class func displayNameExtractor(fileName: String) -> String{
        let fileNameComponent = fileName.characters.split{$0 == "."}.map(String.init)
        return fileNameComponent.first!
    }
    
}

extension String{
    func getPathExtension() -> String{
        return (self as NSString).pathExtension
    }
    
}
