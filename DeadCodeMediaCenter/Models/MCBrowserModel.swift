//
//  MCBrowserModel.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit

class MCBrowserModel: NSObject {
    
    var fileType: MediaType?
    var isSectionOpen = false
    
    var medias: Array<MCMedia>?
    
    init(fileType: MediaType?, medias: Array<MCMedia>?) {
        
        self.fileType = fileType
        self.medias = medias
    }
    
    class func fileIconFinder(mediaType: MediaType) -> String {
        
        switch mediaType {
        case .audio:
            return "file-icon_Audio"
        case .video:
            return "file-icon_Video"
        case .document:
            return "file-icon_PDF"
        case .image:
            return "file-icon_Image"
        case .none:
            return ""
        }
    }

    
}
