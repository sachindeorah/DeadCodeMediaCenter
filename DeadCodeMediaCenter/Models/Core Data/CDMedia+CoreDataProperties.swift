//
//  CDMedia+CoreDataProperties.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 17/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CDMedia {

    @NSManaged var fileName: String?
    @NSManaged var displayName: String?
    @NSManaged var fileType: String?
    @NSManaged var fileExtension: String?
    @NSManaged var iconName: String?
    @NSManaged var isSelectedToPlay: NSNumber?
    @NSManaged var playList: NSManagedObject?

}
