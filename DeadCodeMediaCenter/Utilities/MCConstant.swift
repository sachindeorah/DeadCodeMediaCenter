//
//  MCConstant.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import Foundation

let kImage = "Image"
let kDoc = "Document"
let kVideo = "Video"
let kAudio = "Audio"
let kIncrementor = "Incrementor"
let kAlertMessageForPlaying = "Please select content to play."
let kAlert = "Alert!!!"
let kOK = "OK"
let kPlaylist = "Playlist"
let kBrowserCell = "BrowserCell"
let kMCPlayerViewController = "MCPlayerViewController"
let kReuseIdentifier = "reuseIdentifier"
let kMCPlayerView = "MCPlayerView"

let kFileName = "FILE_NAME"

enum MediaType: String {
    case image = "Image"
    case document = "Document"
    case video = "Video"
    case audio = "Audio"
    case none = "none"
}