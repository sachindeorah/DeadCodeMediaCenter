//
//  DeadCodeMediaCenterTests.swift
//  DeadCodeMediaCenterTests
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import XCTest
@testable import DeadCodeMediaCenter

class DeadCodeMediaCenterTests: XCTestCase {
    
    var viewController: MCFileListViewController!
    
    override func setUp() {
        super.setUp()
        viewController = MCFileListViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
        // Put the code you want to measure the time of here.
        }
    }
    
    func testPerformanceFetchingAllMedia(){
        self.measureBlock {
            //Check the performance of fetching all the files from bundle resources.
            let fileArray =  MCFileManager.getAllFilesFromResourceBundle()
            print(fileArray)
        }
    }
    
    func testFileListsTableViewOutlet() {
        viewController.viewDidLoad()
        XCTAssertNotNil(viewController.fileListTableView)
    }
    
    func testFileListsTableViewCellForRowAtIndexPath() {
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        
        let cell = viewController.tableView(viewController.fileListTableView, cellForRowAtIndexPath: indexPath) as! MCFileTableViewCell
        self.viewController.viewDidLoad()
        XCTAssertEqual(cell.fileName.text!, "Audio")
    }
}
