//
//  DeadCodeMediaCenterUITests.swift
//  DeadCodeMediaCenterUITests
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import XCTest

class DeadCodeMediaCenterUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func PlayFilesAudio(){
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Audio"].tap()
        tablesQuery.staticTexts["Uk Top 40 - Poison"].tap()
        
        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.buttons["▶︎"].tap()
        
        let button = app.buttons["❯"]
        button.tap()
        
        let element = scrollViewsQuery.childrenMatchingType(.Other).element
        element.childrenMatchingType(.Other).elementBoundByIndex(1).buttons["▶︎"].tap()
        button.tap()
        element.tap()
    }
    
    func PlayFilesVideo(){
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Video"].tap()
        tablesQuery.staticTexts["MYSTIC INDIA-SWAMINARAYAN AKSHARDHAM-02"].tap()
        
        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.buttons["▶︎"].tap()
        app.buttons["❯"].tap()
        scrollViewsQuery.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).buttons["▶︎"].tap()
        
    }
    
    func PlayFilesPDF(){
        
        let tablesQuery = XCUIApplication().tables
        tablesQuery.staticTexts["Document"].tap()
        tablesQuery.staticTexts["1"].tap()
        
    }
    
    func PlayFilesImages(){
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Image"].tap()
        tablesQuery.staticTexts["file00053809264"].tap()
    }
    
    func ChangeTab(){
        
        let app = XCUIApplication()
        let tabBarsQuery = app.tabBars
        let playlistsButton = tabBarsQuery.buttons["Playlists"]
        playlistsButton.tap()
        
        let browseFileButton = tabBarsQuery.buttons["Browse File"]
        browseFileButton.tap()
        playlistsButton.tap()
        
        let element = app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
        element.tap()
        browseFileButton.tap()
        element.childrenMatchingType(.Other).element.childrenMatchingType(.Table).element.tap()
        
    }
    
    func testNextPrevButton(){
        
        let app = XCUIApplication()
        app.tables.staticTexts["MYSTIC INDIA-SWAMINARAYAN AKSHARDHAM-02"].tap()
        
        let button = app.buttons["❯"]
        button.tap()
        button.tap()
        app.scrollViews.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(2).childrenMatchingType(.Other).elementBoundByIndex(1).tap()
        
        let button2 = app.buttons["❮ "]
        button2.tap()
        button2.tap()
        button2.tap()
        
    }
    
    func testMultiSelect(){
        
        let app = XCUIApplication()
        app.buttons["Select"].tap()
        
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Audio"].tap()
        tablesQuery.staticTexts["Video"].tap()
        
        let tablesQuery2 = tablesQuery
        tablesQuery2.staticTexts["The Boombastic-Dont Ask Her That"].tap()
        tablesQuery2.staticTexts["MYSTIC INDIA-SWAMINARAYAN AKSHARDHAM-02"].tap()
        tablesQuery.staticTexts["Document"].tap()
        tablesQuery2.staticTexts["100365750372"].tap()
        app.buttons["Play"].tap()
        
        let button = app.buttons["❯"]
        button.tap()
        button.tap()
        app.buttons["❮ BACK"].tap()
        
    }
}
